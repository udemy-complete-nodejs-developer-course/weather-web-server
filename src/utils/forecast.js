const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'https://api.darksky.net/forecast/cc6b292ff02f906700a3e05404c69f5f/' + latitude + ',' + longitude
    
    request({url, json: true}, (error, {body}) => {
        if(error){
            callback('Unable to connect to weather service', undefined)
        } else if(body.error) {
            callback('Unable to find location', undefined)
        } else {
            callback(undefined, 
                // currentTemp: body.currently.temperature,
                // rainProb: body.currently.precipProbability,
                // dailySummary: body.daily.data[0].summary
                body.daily.data[0].summary + 
                ' The current temperature is ' + body.currently.temperature + 
                ' degrees, with a maximum temperature of ' + body.daily.data[0].temperatureMax + 
                ' and a minimum temperature of '+ body.daily.data[0].temperatureMin +
                '. There is a ' + body.currently.precipProbability + '% chance of rain.'
            )
        } 
    })
}

module.exports = forecast