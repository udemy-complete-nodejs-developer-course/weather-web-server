console.log('Client side js file has loaded')

const weatherForm = document.querySelector('form')
const searchQuery = document.querySelector('input')
const messageOne = document.querySelector('#messageOne')
const messageTwo = document.querySelector('#messageTwo')

weatherForm.addEventListener('submit', (e) => {
  e.preventDefault()

  const location = searchQuery.value

  messageOne.textContent = 'Loading...'
  messageTwo.textContent = ''

  fetch('/weather?address=' + location).then((response) => {
    response.json().then((data) => {
      if(data.error) {
        messageOne.textContent = data.error
      } else {
        console.log(data)
        console.log(data.location)
        console.log(data.forecast)

        messageOne.textContent = data.location 
        messageTwo.textContent = data.forecast
      }
    })
  })
})